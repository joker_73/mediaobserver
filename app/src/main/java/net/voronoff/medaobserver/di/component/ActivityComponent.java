package net.voronoff.medaobserver.di.component;

import net.voronoff.medaobserver.data.AuthActivity;
import net.voronoff.medaobserver.di.module.ActivityModule;
import net.voronoff.medaobserver.di.scope.ActivityScope;
import net.voronoff.medaobserver.ui.MainActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
    void inject(AuthActivity authActivity);
}
