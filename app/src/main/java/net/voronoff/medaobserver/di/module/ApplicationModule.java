package net.voronoff.medaobserver.di.module;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import net.voronoff.medaobserver.data.DataManager;
import net.voronoff.medaobserver.di.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private Application mApplication;

    public ApplicationModule(@NonNull Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(@ApplicationContext Context context) {
        return new DataManager(context);
    }
}
