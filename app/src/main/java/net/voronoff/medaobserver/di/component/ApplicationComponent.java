package net.voronoff.medaobserver.di.component;

import net.voronoff.medaobserver.di.module.ActivityModule;
import net.voronoff.medaobserver.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    ActivityComponent plusActivityComponent(ActivityModule activityModule);
}
