package net.voronoff.medaobserver.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import net.voronoff.medaobserver.di.ActivityContext;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private Activity mActivity;

    public ActivityModule(@NonNull Activity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }
}
