package net.voronoff.medaobserver;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import java.util.Collection;

public class Utils {
    public static Drawable getDrawable(Context context, @DrawableRes int res) {
        Drawable drawable;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable = context.getDrawable(res);
        } else {
            //noinspection deprecation
            drawable = context.getResources().getDrawable(res);
        }
        return drawable;
    }

    @NonNull
    public static String join(@NonNull Collection collection,
                              @NonNull String separator,
                              @NonNull String prefix) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean first = true;
        for (Object object : collection) {
            if (first) {
                first = false;
            } else {
                stringBuilder.append(separator);
            }
            stringBuilder.append(prefix).append(object);
        }
        return stringBuilder.toString();
    }
}
