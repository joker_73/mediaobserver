package net.voronoff.medaobserver;

import android.app.Application;

import net.voronoff.medaobserver.di.component.ApplicationComponent;
import net.voronoff.medaobserver.di.component.DaggerApplicationComponent;
import net.voronoff.medaobserver.di.module.ApplicationModule;

public class MediaObserver extends Application {
    private static ApplicationComponent sApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static ApplicationComponent getApplicationComponent() {
        return sApplicationComponent;
    }
}
