package net.voronoff.medaobserver;

public class Constants {
    public static final String INSTAGRAM_CLIENT_ID = "f193fe6cb7b24d0f9ab0076e31e82c3d";
    public static final String REDIRECT_URL = "http://voronoff.net";
    public static final String HASHTAG = "flamp";
    public static final int PAGE_SIZE = 10;
}
