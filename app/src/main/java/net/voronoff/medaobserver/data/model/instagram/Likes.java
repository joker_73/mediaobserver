package net.voronoff.medaobserver.data.model.instagram;

import android.os.Parcel;
import android.os.Parcelable;

public class Likes implements Parcelable {
    public Integer count;

    protected Likes(Parcel in) {
    }

    public static final Creator<Likes> CREATOR = new Creator<Likes>() {
        @Override
        public Likes createFromParcel(Parcel in) {
            return new Likes(in);
        }

        @Override
        public Likes[] newArray(int size) {
            return new Likes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
