package net.voronoff.medaobserver.data.model.instagram;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Data implements Parcelable {
    public List<String> tags;
    public String type;
    public Likes likes;
    public Images images;
    public User user;

    protected Data(Parcel in) {
        tags = in.createStringArrayList();
        type = in.readString();
        likes = in.readParcelable(Likes.class.getClassLoader());
        images = in.readParcelable(Images.class.getClassLoader());
        user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(tags);
        parcel.writeString(type);
        parcel.writeParcelable(likes, i);
        parcel.writeParcelable(images, i);
        parcel.writeParcelable(user, i);
    }
}
