package net.voronoff.medaobserver.data.model.instagram;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Images implements Parcelable {
    @SerializedName("low_resolution")
    public Image lowResolution;

    public Image thumbnail;

    @SerializedName("standard_resolution")
    public Image standardResolution;

    protected Images(Parcel in) {
    }

    public static final Creator<Images> CREATOR = new Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel in) {
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
