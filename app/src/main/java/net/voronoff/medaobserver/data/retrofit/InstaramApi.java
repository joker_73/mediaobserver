package net.voronoff.medaobserver.data.retrofit;

import net.voronoff.medaobserver.data.model.instagram.Response;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface InstaramApi {
    @GET("v1/tags/{tag}/media/recent")
    Observable<Response> getMediaByTag(@Path("tag") String tag,
                                       @Query("access_token") String token,
                                       @Query("count") int count);

    @GET("v1/tags/{tag}/media/recent")
    Observable<Response> getMediaByTag(@Path("tag") String tag,
                                       @Query("access_token") String token,
                                       @Query("count") int count,
                                       @Query("max_tag_id") String maxTagId);

    class Creator {
        public static InstaramApi create() {
            return new Retrofit.Builder()
                    .baseUrl("https://api.instagram.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build()
                    .create(InstaramApi.class);
        }
    }
}
