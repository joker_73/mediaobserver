package net.voronoff.medaobserver.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.voronoff.medaobserver.data.model.instagram.Response;
import net.voronoff.medaobserver.data.retrofit.InstaramApi;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DataManager {
    private TokenManager mTokenManager;

    private InstaramApi mInstaramApi;

    public DataManager(@NonNull Context context) {
        mTokenManager = new TokenManager(context);
        mInstaramApi = InstaramApi.Creator.create();
    }

    public void dropToken() {
        mTokenManager.drop();
    }

    @SuppressWarnings("WeakerAccess")
    public Observable<Response> getDataByTag(@NonNull String tag, int count) {
        return mTokenManager.getToken()
                .flatMap(token -> mInstaramApi.getMediaByTag(tag, token, count))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response> getDataByTag(@NonNull String tag,
                                             int count,
                                             @Nullable String maxTagId) {
        Observable<Response> responseObservable;
        if (maxTagId == null) {
            responseObservable = getDataByTag(tag, count);
        } else {
            responseObservable = mTokenManager.getToken()
                    .flatMap(token -> mInstaramApi.getMediaByTag(tag, token, count, maxTagId))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
        return responseObservable;
    }
}
