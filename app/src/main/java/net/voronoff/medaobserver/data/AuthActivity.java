package net.voronoff.medaobserver.data;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.InputStreamReader;

import static net.voronoff.medaobserver.Constants.INSTAGRAM_CLIENT_ID;
import static net.voronoff.medaobserver.Constants.REDIRECT_URL;

public class AuthActivity extends Activity {
    public static final String AUTH_ACTIVITY_ACTION_DESTROY = "custom.intent.auth.destroyed";
    public static final String TOKEN_SP = "token";
    public static final String TOKEN_KEY = "TOKEN_KEY";

    private Uri mAuthUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webView = new WebView(this);
        webView.setWebViewClient(new CustomWebViewClient());
        //noinspection deprecation
        webView.getSettings().setSavePassword(false);

        setContentView(webView);

        mAuthUri = Uri.parse("https://api.instagram.com/oauth/authorize/?response_type=token")
                .buildUpon()
                .appendQueryParameter("scope", "public_content")
                .appendQueryParameter("redirect_uri", REDIRECT_URL)
                .appendQueryParameter("client_id", INSTAGRAM_CLIENT_ID)
                .build();

        clearCookies();

        webView.loadUrl(mAuthUri.toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        sendBroadcast(new Intent(AUTH_ACTIVITY_ACTION_DESTROY));
        finish();
    }

    @SuppressWarnings("deprecation")
    private void clearCookies() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
            cookieSyncManager.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }

    private void save(@NonNull String token) {
        SharedPreferences sharedPreferences = getSharedPreferences(TOKEN_SP, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    private class CustomWebViewClient extends WebViewClient {
        private boolean handleUrl(WebView view, String url) {
            if (url.startsWith(REDIRECT_URL)) {
                Uri uri = Uri.parse(url);
                save(uri.getEncodedFragment().split("=")[1]);
                clearCookies();
                finish();
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return handleUrl(view, url);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return handleUrl(view, request.getUrl().toString());
        }

        @Override
        public void onReceivedHttpError(WebView view,
                                        WebResourceRequest request,
                                        WebResourceResponse errorResponse) {
            if (errorResponse != null &&
                    errorResponse.getData() != null &&
                    "application/json".equals(errorResponse.getMimeType())) {
                InputStreamReader reader = new InputStreamReader(errorResponse.getData());
                ErrorResponse error = new Gson().fromJson(reader, ErrorResponse.class);
                new AlertDialog.Builder(AuthActivity.this)
                        .setTitle(error.errorType)
                        .setMessage(error.errorMessage)
                        .show();
            }
            clearCookies();
            view.loadUrl(mAuthUri.toString());
        }
    }

    private static class ErrorResponse {
        @SuppressWarnings("unused")
        Integer code;

        @SerializedName("error_type")
        String errorType;

        @SerializedName("error_message")
        String errorMessage;
    }
}
