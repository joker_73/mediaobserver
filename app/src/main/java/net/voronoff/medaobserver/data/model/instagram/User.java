package net.voronoff.medaobserver.data.model.instagram;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {
    public String username;

    @SerializedName("profile_picture")
    public String profilePicture;

    public String id;

    @SerializedName("full_name")
    public String fullName;

    protected User(Parcel in) {
        username = in.readString();
        profilePicture = in.readString();
        id = in.readString();
        fullName = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(profilePicture);
        parcel.writeString(id);
        parcel.writeString(fullName);
    }
}
