package net.voronoff.medaobserver.data.model.instagram;

import java.util.List;

public class Response {
    public List<Data> data;
    public Pagination pagination;
}
