package net.voronoff.medaobserver.data.model.instagram;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("next_max_tag_id")
    public String nextMaxTagId;

    @SerializedName("deprecation_warning")
    public String deprecationWarning;

    @SerializedName("next_max_id")
    public String nextMaxId;

    @SerializedName("next_min_id")
    public String nextMinId;

    @SerializedName("min_tag_id")
    public String minTagId;

    @SerializedName("next_url")
    public String nextUrl;

    @Override
    public String toString() {
        return String.format("{\n" +
                        "\tnext_max_tag_id: %s\n" +
                        "\tdeprecation_warning: %s\n" +
                        "\tnext_max_id: %s\n" +
                        "\tnext_min_id: %s\n" +
                        "\tmin_tag_id: %s\n" +
                        "\tnext_url: %s\n" +
                        "}",
                nextMaxTagId, deprecationWarning, nextMaxId, nextMinId, minTagId, nextUrl);
    }
}
