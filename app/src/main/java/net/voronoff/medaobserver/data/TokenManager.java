package net.voronoff.medaobserver.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import rx.Observable;
import rx.subjects.BehaviorSubject;

import static net.voronoff.medaobserver.data.AuthActivity.AUTH_ACTIVITY_ACTION_DESTROY;
import static net.voronoff.medaobserver.data.AuthActivity.TOKEN_KEY;
import static net.voronoff.medaobserver.data.AuthActivity.TOKEN_SP;

class TokenManager {
    private SharedPreferences mSharedPreferences;
    private String mToken;
    private Context mContext;

    TokenManager(Context context) {
        mContext = context;
        mSharedPreferences = context.getSharedPreferences(TOKEN_SP, Context.MODE_PRIVATE);
    }

    void drop() {
        mToken = null;
        if (mSharedPreferences.contains(TOKEN_KEY)) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.remove(TOKEN_KEY);
            editor.apply();
        }
    }

    Observable<String> getToken() {
        load();
        BehaviorSubject<String> subject = BehaviorSubject.create();
        if (mToken == null) {
            mContext.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    mContext.unregisterReceiver(this);
                    if (mToken == null) {
                        subject.onError(new TokenNotFound());
                    } else {
                        subject.onNext(mToken);
                    }
                }
            }, new IntentFilter(AUTH_ACTIVITY_ACTION_DESTROY));
            Intent intent = new Intent(mContext, AuthActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } else {
            subject.onNext(mToken);
        }
        return subject;
    }

    private void load() {
        if (mToken == null) {
            mToken = mSharedPreferences.getString(TOKEN_KEY, null);
        }
    }
}
