package net.voronoff.medaobserver.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.voronoff.medaobserver.R;
import net.voronoff.medaobserver.data.model.instagram.Data;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.voronoff.medaobserver.Utils.getDrawable;
import static net.voronoff.medaobserver.Utils.join;


public class DataFragment extends Fragment {
    @BindView(R.id.image)
    ImageView mImage;

    @BindView(R.id.like)
    ImageView mLike;

    @BindView(R.id.like_num)
    TextView mLikeNum;

    @BindView(R.id.tags)
    TextView mTags;

    private static final String DATA_ARG = "DATA_ARG";

    private Data mData;

    public static DataFragment newInstance(@NonNull Data data) {
        DataFragment imageFragment = new DataFragment();
        Bundle args = new Bundle();
        args.putParcelable(DATA_ARG, data);
        imageFragment.setArguments(args);
        return imageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable(DATA_ARG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instagram_data, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Picasso.with(getActivity())
                .load(mData.images.standardResolution.url)
                .error(R.drawable.ic_error)
                .into(mImage);
        if (mData.likes.count.equals(0)) {
            mLike.setImageDrawable(getDrawable(getActivity(), R.drawable.ic_like_border));
            mLikeNum.setVisibility(View.INVISIBLE);
        } else {
            mLike.setImageDrawable(getDrawable(getActivity(), R.drawable.ic_like));
            mLikeNum.setVisibility(View.VISIBLE);
            mLikeNum.setText(String.valueOf(mData.likes.count));
        }
        mTags.setText(join(mData.tags, " ", "#"));
    }
}
