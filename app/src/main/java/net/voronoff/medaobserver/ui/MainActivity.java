package net.voronoff.medaobserver.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.jakewharton.rxbinding.support.v4.view.RxViewPager;

import net.voronoff.medaobserver.MediaObserver;
import net.voronoff.medaobserver.R;
import net.voronoff.medaobserver.data.DataManager;
import net.voronoff.medaobserver.data.model.instagram.Response;
import net.voronoff.medaobserver.di.module.ActivityModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;

import static net.voronoff.medaobserver.Constants.HASHTAG;
import static net.voronoff.medaobserver.Constants.PAGE_SIZE;

public class MainActivity extends AppCompatActivity {
    @Inject
    DataManager mDataManager;

    @BindView(R.id.pager)
    ViewPager mPager;

    private DataFragmentPagerAdapter mAdapter;

    private Subscription mLoadSubscription = null;

    private String mNextMaxTagId = null;
    private boolean mHasNextPage = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MediaObserver.getApplicationComponent()
                .plusActivityComponent(new ActivityModule(this))
                .inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mAdapter = new DataFragmentPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        load(HASHTAG, PAGE_SIZE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unsubsidisedLoad();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_exit:
            mDataManager.dropToken();
            recreate();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void load(@NonNull String hashTag, int pageSize) {
        unsubsidisedLoad();
        mLoadSubscription = RxViewPager.pageSelections(mPager)
                .filter(integer -> mHasNextPage && (integer + 3) > mAdapter.getCount())
                .flatMap(integer -> mDataManager.getDataByTag(hashTag, pageSize, mNextMaxTagId))
                .doOnNext(this::retrievePaginationState)
                .doOnNext(response -> sanityCheck(response, hashTag))
                .flatMap(response -> Observable.from(response.data))
                .filter(data -> "image".equals(data.type))
                .subscribe(mAdapter::add, Throwable::printStackTrace);
    }

    private void retrievePaginationState(@NonNull Response response) {
        mNextMaxTagId = response.pagination.nextMaxTagId;
        mHasNextPage = response.pagination.nextUrl != null;
    }

    private void sanityCheck(@NonNull Response response, @NonNull String hashTag) {
        if (response.data.size() == 0 && mAdapter.getCount() == 0) {
            String message = String.format(getString(R.string.not_found_message), hashTag);
            Snackbar.make(mPager, message, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    private void unsubsidisedLoad() {
        if (mLoadSubscription != null && !mLoadSubscription.isUnsubscribed()) {
            mLoadSubscription.unsubscribe();
        }
    }
}
