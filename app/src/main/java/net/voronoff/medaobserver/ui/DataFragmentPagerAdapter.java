package net.voronoff.medaobserver.ui;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.voronoff.medaobserver.data.model.instagram.Data;

import java.util.LinkedList;
import java.util.List;

class DataFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<Data> mData = new LinkedList<>();

    DataFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return DataFragment.newInstance(mData.get(position));
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @SuppressWarnings("WeakerAccess")
    public void add(@NonNull Data data) {
        mData.add(data);
        notifyDataSetChanged();
    }
}
